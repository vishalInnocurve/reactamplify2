import React from "react";
import MyProfile from "./popups/MyProfile";
import { connect } from "react-redux";
import Modal from "@material-ui/core/Modal";
import Draggable from "react-draggable";
class StudentCorner extends React.Component {
  state = {
    myProfile: false,
  };

  myProfile = () => {
    this.setState({
      myProfile: true,
    });
  };

  render() {
    let data = this.props.userData;
    let name = data["UserName"];
    return (
      <div className="main-content bg-light">
        <Modal
          open={this.state.myProfile}
          onClose={() => this.setState({ myProfile: false })}
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
        >
          <Draggable>
            <div
              tabIndex="-1"
              style={{
                paddingLeft: 600,
                paddingTop: 300,
                width: 250,
              }}
            >
              <MyProfile myProfile={data} />
            </div>
          </Draggable>
        </Modal>
        <header>
          <h4>
            <label htmlFor="nav-toggel">
              <span>
                <i className="fa fa-bars" aria-hidden="true"></i>
              </span>
            </label>
            <span className="name">StudentCorner</span>
          </h4>

          <div className="search-wrapper">
            <span>
              <i className="fa fa-search" aria-hidden="true"></i>
            </span>
            <input type="search" placeholder="Search here" />
          </div>

          <div className="user-wrapper" onClick={this.myProfile}>
            <img
              src={this.props.profileImageUrl}
              width="50px"
              height="50px"
              alt=""
            />
            <div>
              <h6>{name}</h6>
              <small>Admin</small>
            </div>
          </div>
        </header>

        <main>
          <div className="filter-wrapper"></div>

          <div className="table-content m-3">
            <p>StudentCorner</p>
          </div>
        </main>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isAuthenticated: state.isAuthenticated,
    userData: state.userData,
    profileImageUrl: state.studentImage.data,
  };
};
export default connect(mapStateToProps, null)(StudentCorner);

//export default StudentCorner;
