import moment from "moment";
import React from "react";
import { Field, reduxForm } from "redux-form";
import { connect } from "react-redux";
import { API, graphqlOperation } from "aws-amplify";
import validator from "validator";
import AlertComponent from "../../auth/AlertComponent";
import { alertActionTeacherCreation, removeAlert } from "../../../actions";
import { createTeacher } from "../../../actions/appSyncActions";
// import { okPopUp } from "./okPopUp";
// import Modal from "@material-ui/core/Modal";
// import Draggable from "react-draggable";
// import moment from "moment";
const mutationCreateTeacher = `mutation abcd($input:teacherCreationInput){
  createTeacher(input: $input){
    email
  	name
  	phoneNumber
  	adminEmail
  	adminCode
   	registrationNumber
  	rollNo
  	dateOfbirth
  	error
  }
}`;

const validate = (formValues) => {
  let error = {};
  if (!formValues.email) {
    error.email = "Please provide an Email";
  }
  if (!formValues.name) {
    error.name = "Please provide a name";
  }
  if (formValues.email && !validator.isEmail(formValues.email)) {
    error.email = "* Please provide a valid email";
  }
  if (!formValues.phoneNumber) {
    //console.log("formValues.phone_number", formValues.phone_number);
    error.phoneNumber = "Please provide a Phone Name";
  }
  if (formValues.dateOfbirth) {
    console.log("check2");
    var a = moment(formValues.dateOfbirth);
    var b = moment(new Date());
    console.log("a.diff ", b.diff(a, "years"));
    if (b.diff(a, "years") < 16) {
      error.dateOfbirth = "Please provide a teacher age of more than 15 years";
    }
    if (b.diff(a, "years") > 50) {
      error.dateOfbirth = "Please provide a teacher age of less than 50 years";
    }
  }
  if (formValues.phoneNumber && formValues.phoneNumber.length !== 10) {
    //console.log("formValues.phone_number", formValues.phone_number);
    error.phoneNumber = "* Please provide a 10 digit Phone Name";
  }
  if (!formValues.registrationNumber) {
    error.registrationNumber = "Please provide a registrationNumber";
  }
  if (!formValues.parentPhoneNumber) {
    error.parentPhoneNumber = "Please provide a Parent Phone Number";
  }
  if (!formValues.rollNo) {
    error.rollNo = "Please provide a rollNo";
  }
  if (!formValues.dateOfbirth) {
    error.dateOfbirth = "Please provide a dateOfbirth";
  }
  return error;
};

class CreateTeachers extends React.Component {
  renderError({ error, touched }) {
    //console.log("error, touched", error, touched);
    if (touched && error) {
      return (
        <div className="input-group">
          <div
            className="input-group-prepend"
            style={{ backgroundColor: "red" }}
          ></div>
          <p style={{ color: "red" }}>{error}</p>
        </div>
      );
    }
  }

  renderEmailInput = ({ input, placeholder, meta, type, label, iconClass }) => {
    //console.log("formValues", formProps);

    return (
      <div className="form-group">
        <label>{label}</label>
        <div className="input-group">
          <input
            className="form-control"
            name="code"
            type={type}
            placeholder={placeholder}
            {...input}
          />
        </div>
        {this.renderError(meta)}
      </div>
    );
  };

  renderPhoneInput = ({ input, placeholder, meta, type, label, disabled }) => {
    //console.log("formValues", formProps);

    return (
      <div className="form-group password-field">
        <label>{label}</label>
        <div className="input-group">
          <div className="input-group-prepend"></div>
          <input
            required=""
            className="form-control"
            pattern="^[1-9][0-9]{9}$"
            type={type}
            placeholder={placeholder}
            {...input}
            disabled={disabled}
          />
        </div>
        {this.renderError(meta)}
      </div>
    );
  };

  onSubmit = async (formValues) => {
    console.log("formvalues", formValues);

    try {
      let input2 = {
        email: formValues.email,
        name: formValues.name,
        phoneNumber: "+" + formValues.phoneNumber,
        adminEmail: this.props.userData.UserEmail,
        adminCode: this.props.userData.UniCode,
        registrationNumber: formValues.registrationNumber,
        rollNo: formValues.rollNo,
        dateOfbirth: formValues.dateOfbirth,
      };
      console.log("input2", input2);
      const result = await API.graphql(
        graphqlOperation(mutationCreateTeacher, { input: input2 }) //this is actually creating teachers
      );
      console.log("result", result);
      if (result.data.createTeacher.error === "User Already Exists") {
        //we basically have to create a pop to tell information was not saved.
      } else {
        this.props.createTeacher({
          ...result.data.createTeacher,
          userdob: result.data.createTeacher.dateOfbirth,
        });
        this.props.addPopUp();
      }
      try {
        if (result.data.createTeacher.error !== null) {
          this.props.alertActionTeacherCreation("Email already exists");
        }
      } catch (e) {
        console.log("e", e);
        this.props.alertActionTeacherCreation("Email already exists");
      }

      setTimeout(() => {
        this.props.removeAlert();
      }, 5000);
    } catch (e) {
      console.log("e", e);
    }
  };
  render() {
    return (
      <div>
        <div
          onClick={(e) => e.stopPropagation()}
          className="ui standard modal visible active"
        >
          <div className="header">Teacher Creation</div>
          <div className="content">
            <form onSubmit={this.props.handleSubmit(this.onSubmit)}>
              <Field
                label="Teacher email"
                type="email"
                name="email"
                component={this.renderEmailInput}
                placeholder="Email"
              />
              <Field
                label="Teacher name"
                type="text"
                name="name"
                component={this.renderEmailInput}
                placeholder="name"
              />
              <Field
                label="Phone number"
                type="text"
                name="phoneNumber"
                component={this.renderPhoneInput}
                placeholder="phoneNumber"
              />
              <Field
                label="DOB"
                type="date"
                name="dateOfbirth"
                component={this.renderEmailInput}
                placeholder="dateOfbirth"
              />
              <Field
                label="Roll No"
                type="text"
                name="rollNo"
                component={this.renderEmailInput}
                placeholder="Roll Number"
              />
              <Field
                label="Registration number"
                type="text"
                name="registrationNumber"
                component={this.renderEmailInput}
                placeholder="Registration number"
              />
              <button className="ui button" type="submit">
                Submit
              </button>
              <AlertComponent />
            </form>
          </div>
        </div>
      </div>
    );
  }
}

const formWrapped = reduxForm({
  form: "create Teachers",
  validate,
})(CreateTeachers);

const mapStateToProps = (state) => {
  return {
    isAuthenticated: state.isAuthenticated,
    userData: state.userData,
  };
};

export default connect(mapStateToProps, {
  alertActionTeacherCreation,
  removeAlert,
  createTeacher,
})(formWrapped);
