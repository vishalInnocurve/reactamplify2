import React from "react";
// import { connect } from "react-redux";

class OkPopUp extends React.Component {
  onButton = () => {
    this.props.removePopUp();
  };
  render() {
    console.log("render render");
    return (
      <div>
        <div
          onClick={(e) => e.stopPropagation()}
          className="ui standard modal visible active"
        >
          <div className="header">{this.props.headerInfo}</div>
          <div className="content">
            <button
              className="btn btn-primary mb-2"
              onClick={() => this.onButton()}
            >
              Ok
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default OkPopUp;
