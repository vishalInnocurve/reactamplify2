/*
this component displays student related information
DeleteStudent -> to delete the students directly calls the deleteStudent mutation 
                  which deletes data from cognito and database.

EditStudents -> calls updateStudent mutation and updates the particular mutation.this also gets passed 
                Unicode of Admin,the refresh data of all the students data when a student gets updated.
                

*/

import moment from "moment";
import React from "react";
import { Redirect } from "react-router-dom";
import MyProfile from "./popups/MyProfile";
import { API, graphqlOperation } from "aws-amplify";
import { connect } from "react-redux";
import Modal from "@material-ui/core/Modal";
import Draggable from "react-draggable";
import CreateTeachers from "./popups/createTeachers";
import { deleteTeacherMutation, updateTeacherMutation } from "../../mutation";
import { loadTeacherData } from "../../actions";
import { Field, reduxForm } from "redux-form";
import OkPopUp from "./popups/okPopUp";

import {
  reloadTeachers,
  removeTeacher,
  updateTeacher,
} from "../../actions/appSyncActions";

class DeleteStudent extends React.Component {
  deleteStudent = async (email) => {
    try {
      console.log({ email, code: this.props.UniCode });
      const deleteTeacher = await API.graphql(
        graphqlOperation(deleteTeacherMutation, {
          input: { email, code: "TR002" },
        })
      );
      console.log("deleteTeacher", deleteTeacher);
      this.props.removeTeacher(email);
    } catch (e) {
      console.log("error while deleting student", e);
    }
    this.props.removePopUp();
  };

  render() {
    console.log("this.props.myProfile", this.props.myProfile);
    return (
      <div>
        <div className="content">
          <p>Are you sure you want to delete {this.props.myProfile.name} ? </p>
          <button
            className="ui button"
            onClick={() => this.deleteStudent(this.props.myProfile.email)}
          >
            Delete {this.props.myProfile.name}
          </button>
        </div>
      </div>
    );
  }
}

const mp = (state) => {
  return {
    UniCode: state.userData.UniCode,
  };
};

const DeleteStudentRedux = connect(mp, { removeTeacher })(DeleteStudent);

const validate = (formValues) => {
  let error = {};
  if (!formValues.name) {
    error.name = "* Please provide a full Name";
  }
  if (!formValues.phone_number) {
    error.phone_number = "Please provide a Phone Name";
  }
  if (formValues.phone_number && formValues.phone_number.length !== 10) {
    error.phone_number = "* Please provide a 10 digit Phone Name";
  } //parentName
  if (formValues.userdob) {
    console.log("check2");
    var a = moment(formValues.userdob);
    var b = moment(new Date());
    console.log("a.diff ", b.diff(a, "years"));
    if (b.diff(a, "years") < 16) {
      error.userdob = "Please provide a student age of more than 15 years";
    }
    if (b.diff(a, "years") > 50) {
      error.dateOfbirth = "Please provide a student age of less than 50 years";
    }
  }
  if (!formValues.studentDoB) {
    console.log("check1", formValues.studentDoB);
    // var a = moment(formValues.studentDoB);
    // var b = moment(new Date());
    // console.log("a.diff ", a.diff(b, "years"));
    // error.studentDoB = "Please provide a Phone Name 15 years";
  }

  return error;
};

/*
the form to edit the students profile.
this component also gets passed the univeristyid(UniCode) of the admin
so that whenever any students data gets changed its calls the 
student data query and reload student data
*/
class EditStudents extends React.Component {
  componentDidMount() {
    this.props.loadTeacherData(this.props.myProfile);
    console.log("A student profile for edit opened", this.props.myProfile);
  }

  renderError({ error, touched }) {
    if (touched && error) {
      return (
        <div className="input-group">
          <div
            className="input-group-prepend"
            style={{ backgroundColor: "red" }}
          ></div>
          <p style={{ color: "red" }}>{error}</p>
        </div>
      );
    }
  }

  onSubmit = async (formValues) => {
    var inputUpdateStudent = {
      studentName: formValues.name,
      studentRollNo: formValues.rollNo,
      studentEmail: formValues.username,
      studentRegistrationNumber: formValues.registrationNumber,
      studentPhoneNumber: "+" + formValues.phone_number,
      studentDoB: moment(formValues.userdob).format("YYYY-MM-DD"),
      adminCode: this.props.UniCode,
    };
    try {
      const result = await API.graphql(
        graphqlOperation(updateTeacherMutation, { input: inputUpdateStudent })
      );
      console.log("edit student is called", result.data.updateTeacher);
      this.props.updateTeacher(result.data.updateTeacher);

      this.props.addConfirmPopUp();
    } catch (e) {
      console.log("e", e);
    }
  };

  renderEmailInput = ({ input, placeholder, meta, type, label, disabled }) => {
    return (
      <div className="form-group">
        <label>{label}</label>
        <div className="input-group">
          <div className="input-group-prepend"></div>
          <input
            className="form-control"
            type={type}
            placeholder={placeholder}
            {...input}
            disabled={disabled}
          />
        </div>
        {this.renderError(meta)}
      </div>
    );
  };

  renderPhoneInput = ({ input, placeholder, meta, type, label, disabled }) => {
    //console.log("formValues", formProps);

    return (
      <div className="form-group password-field">
        <label>{label}</label>
        <div className="input-group">
          <div className="input-group-prepend"></div>
          <input
            required=""
            className="form-control"
            pattern="^[1-9][0-9]{9}$"
            type={type}
            placeholder={placeholder}
            {...input}
            disabled={disabled}
          />
        </div>
        {this.renderError(meta)}
      </div>
    );
  };

  render() {
    return (
      <form onSubmit={this.props.handleSubmit(this.onSubmit)}>
        <Field
          label="Teacher Name"
          type="text"
          name="name"
          component={this.renderEmailInput}
          placeholder="Teacher Name"
          disabled={false}
        />
        <Field
          label="Teacher Email"
          type="text"
          name="username"
          component={this.renderEmailInput}
          placeholder="Teacher Email"
          disabled={true}
        />
        <Field
          label="Phone Number"
          type="text"
          name="phone_number"
          component={this.renderPhoneInput}
          placeholder="xxxxxxxxxx"
          disabled={false}
        />
        <Field
          label="User Dob"
          type="date"
          name="userdob"
          component={this.renderEmailInput}
          placeholder="User DoB"
          disabled={false}
        />
        <Field
          label="Roll No"
          type="text"
          name="rollNo"
          component={this.renderEmailInput}
          placeholder="Roll No"
          disabled={false}
        />
        <Field
          label="Registration Number"
          type="text"
          name="registrationNumber"
          component={this.renderEmailInput}
          placeholder="Registration Number"
          disabled={false}
        />
        <div className="row">
          <div className="col-12">
            <button
              // disabled={!this.props.valid}
              type="submit"
              className="btn btn-primary btn-block"
            >
              Save
            </button>
          </div>
        </div>
      </form>
    );
  }
}

const FormWrappedEditStudent = reduxForm({
  form: "edit student profile",
  validate,
  enableReinitialize: true,
})(EditStudents);

const mapStateToPropsEditStudents = (state) => {
  console.log("state edit students", state.studentProfileValues.data);
  if (state.studentProfileValues.data) {
    return {
      initialValues: {
        name: state.studentProfileValues.data.name || "",
        username: state.studentProfileValues.data.email || "",
        phone_number: state.studentProfileValues.data.phoneNumber
          ? state.studentProfileValues.data.phoneNumber.toString().slice(1, 11)
          : "",
        registrationNumber:
          state.studentProfileValues.data.registrationNumber || "",
        rollNo: state.studentProfileValues.data.rollNo || "",
        userdob: moment(state.studentProfileValues.data.userdob).format(
          "YYYY-MM-DD"
        ), //.slice(0, 10)
      },
      UniCode: state.userData.UniCode,
    };
  } else {
    return {
      initialValues: {},
      UniCode: state.userData.UniCode,
    };
  }
};

const StudentEditProfile = connect(mapStateToPropsEditStudents, {
  loadTeacherData,
  reloadTeachers,
  updateTeacher,
})(FormWrappedEditStudent);

/* this is baiscally the popup when we wan to edit or delete profile.
   the last column in each student profile */

class StudentsProfile extends React.Component {
  state = {
    deleteprofile: "",
    editProfileStudent: "name",
  };
  someFunct = (name) => {
    this.setState({
      deleteprofile: "",
      editProfileStudent: "",
      [name]: "name",
    });
  };

  render() {
    return (
      <div>
        <div className="ui standard modal visible active">
          <div className="ui secondary pointing menu">
            <button
              className={
                this.state.deleteprofile === "name" ? "item active" : "item"
              }
              onClick={() => this.someFunct("deleteprofile")}
            >
              DeleteTeacher
            </button>
            <button
              className={
                this.state.editProfileStudent === "name"
                  ? "item active"
                  : "item"
              }
              onClick={() => this.someFunct("editProfileStudent")}
            >
              Edit Teacher Profile
            </button>
          </div>
          <div className="ui segment">
            {this.state.deleteprofile === "name" ? (
              <DeleteStudentRedux
                removePopUp={this.props.removePopUp}
                myProfile={this.props.myProfile}
              />
            ) : (
              <React.Fragment />
            )}
            {this.state.editProfileStudent === "name" ? (
              <StudentEditProfile
                myProfile={this.props.myProfile}
                addConfirmPopUp={this.props.addConfirmPopUp}
              />
            ) : (
              <React.Fragment />
            )}
          </div>
        </div>
      </div>
    );
  }
}

//this represents all the students data
class TeachingStaffData extends React.Component {
  state = {
    myProfile: false,
    addStudents: false,
    posts: [],
    editStudentPopUp: false,
    saveStudentProfile: false,
    studentEmail: "",
    popUpInformation: "",
  };

  saveStudentProfileConfirm = () => {
    console.log("saveStudentProfileConfirm");
    this.setState({
      editStudentPopUp: false,
      saveStudentProfile: true,
      addStudents: false,
      popUpInformation: "Information Saved",
    });
  };

  saveStudentCreationConfirm = () => {
    console.log("saveStudentCreaionConfirm");
    this.setState({
      editStudentPopUp: false,
      saveStudentProfile: true, //application for create student profile also
      addStudents: false,
      popUpInformation: "New Student Created",
    });
  };

  removeStudentProfileConfirm = () => {
    this.setState({
      editStudentPopUp: false,
      saveStudentProfile: false,
      addStudents: false,
    });
  };

  myProfile = () => {
    this.setState({
      myProfile: true,
    });
  };

  componentDidMount() {
    console.log("componentDidMount reloadTeachers");
    this.props.reloadTeachers(this.props.userData.UniCode);
  }

  /* this will be called everytime any student data gets updated, 
     remember evrytime,whenever a student is updated,
     the reload students action inappsyncactions gets called,to bring all the students data.     */
  componentDidUpdate = (previousProps) => {
    if (previousProps.teacherData !== this.props.teacherData) {
      console.log("component did update ");
      this.setState({ posts: this.props.teacherData.data });
    }
    if (previousProps.removeTeacher !== this.props.removeTeacher) {
      console.log("remove student from the object", this.props.removeTeacher);
      let postsArray = this.state.posts.filter((item) => {
        return item.email !== this.props.removeTeacher.data;
      });
      this.setState({ posts: postsArray });
    }
    if (previousProps.updateTeacher !== this.props.updateTeacher) {
      console.log("updateTeacher from the object", this.props.updateTeacher);
      let updatedItem = this.props.updateTeacher.data;
      let posts = this.state.posts;
      for (var i in posts) {
        if (posts[i].email === updatedItem.email) {
          console.log(posts[i]);
          posts[i].name = updatedItem.name;
          posts[i].phoneNumber = updatedItem.phoneNumber;
          posts[i].registrationNumber = updatedItem.registrationNumber;
          posts[i].rollNo = updatedItem.rollNo;
          posts[i].userdob = updatedItem.dateOfbirth;
          break; //Stop this loop, we found it!
        }
      }

      this.setState({ posts });
    }

    if (previousProps.createTeacher !== this.props.createTeacher) {
      console.log("create student from the object", this.props.createTeacher);
      let posts = this.state.posts;
      posts.push(this.props.createTeacher.data);
      this.setState({ posts });
    }
  };

  editStudentsAddPopUp = (email) => {
    console.log("edit students add popup", email);
    this.setState({
      editStudentPopUp: true,
      studentEmail: email,
    });
  };

  editStudentsRemovePopUp = (email) => {
    this.setState({
      editStudentPopUp: false,
      popUpInformation: "New Student Created",
    });
  };

  renderStudents(saveStudentProfileConfirm) {
    var posts = this.state.posts;
    return posts.map((item) => {
      let { name, email } = item;
      return (
        <tr key={email}>
          <Modal
            open={
              this.state.editStudentPopUp && this.state.studentEmail === email
            }
            onClose={() => this.setState({ editStudentPopUp: false })}
            aria-labelledby="simple-modal-title"
            aria-describedby="simple-modal-description"
            aria-hidden="true"
          >
            <Draggable>
              <div
                tabIndex="-1"
                style={{
                  paddingLeft: 600,
                  paddingTop: 200,
                  width: 250,
                }}
              >
                <StudentsProfile
                  myProfile={item}
                  removePopUp={this.editStudentsRemovePopUp}
                  addConfirmPopUp={saveStudentProfileConfirm}
                />
              </div>
            </Draggable>
          </Modal>
          <td>
            <div className="form-check">
              <input
                className="form-check-input position-static"
                type="checkbox"
                id="blankCheckbox"
                value="option1"
                aria-label="..."
              />
            </div>
          </td>
          <td>
            <span>
              <img
                src="../image/teacher.png"
                width="50px"
                height="50px"
                alt=""
              />
            </span>
            <span>{name}</span>
          </td>
          <td>{email}</td>
          <td>Maths</td>
          <td>Professor</td>
          <td>Evening</td>
          <td>
            <button onClick={() => this.editStudentsAddPopUp(email)}>
              <i className="fa fa-ellipsis-h" aria-hidden="true"></i>
            </button>
          </td>
        </tr>
      );
    });
  }

  render = () => {
    if (this.props.isAuthenticated) {
      let data = this.props.userData;
      let name = data["UserName"];
      let universityName = data["Uni_Name"];
      return (
        <div className="main-content bg-light">
          <header>
            <h4>
              <label htmlFor="nav-toggel">
                <span>
                  <i className="fa fa-bars" aria-hidden="true"></i>
                </span>
              </label>
              <span className="name">Teacher Data</span>
            </h4>

            <div className="search-wrapper">
              <span>
                <i className="fa fa-search" aria-hidden="true"></i>
              </span>
              <input type="search" placeholder="Search here" />
            </div>

            <div className="user-wrapper" onClick={this.myProfile}>
              <img
                src={this.props.profileImageUrl}
                width="50px"
                height="50px"
                alt=""
              />
              <div>
                <h6>{name}</h6>
                <small>Admin</small>
              </div>
            </div>
          </header>

          <Modal
            open={this.state.saveStudentProfile}
            onClose={() => this.setState({ saveStudentProfile: false })}
            aria-labelledby="simple-modal-title"
            aria-describedby="simple-modal-description"
            aria-hidden="true"
          >
            <Draggable>
              <div
                tabIndex="-1"
                style={{
                  paddingLeft: 600,
                  paddingTop: 300,
                  width: 250,
                }}
              >
                <OkPopUp
                  removePopUp={this.removeStudentProfileConfirm}
                  headerInfo={this.state.popUpInformation}
                />
              </div>
            </Draggable>
          </Modal>
          <Modal
            open={this.state.myProfile}
            onClose={() => this.setState({ myProfile: false })}
            aria-labelledby="simple-modal-title"
            aria-describedby="simple-modal-description"
            aria-hidden="true"
          >
            <Draggable>
              <div
                tabIndex="-1"
                style={{
                  paddingLeft: 600,
                  paddingTop: 200,
                  width: 250,
                }}
              >
                <MyProfile myProfile={data} />
              </div>
            </Draggable>
          </Modal>
          <Modal
            open={this.state.addStudents}
            onClose={() => this.setState({ addStudents: false })}
            aria-labelledby="simple-modal-title"
            aria-describedby="simple-modal-description"
            aria-hidden="true"
          >
            <Draggable>
              <div
                tabIndex="-1"
                style={{
                  paddingLeft: 600,
                  paddingTop: 300,
                  width: 250,
                }}
              >
                <CreateTeachers addPopUp={this.saveStudentCreationConfirm} />
              </div>
            </Draggable>
          </Modal>

          <main>
            <div className="filter-wrapper">
              <div className="filter">
                <label>Stream</label>
                <input
                  type="text"
                  className="form-control"
                  placeholder="e.g. computer"
                />
              </div>
              <div className="filter">
                <label>Batch</label>
                <input
                  type="text"
                  className="form-control"
                  placeholder="e.g. morning"
                />
              </div>
              <div className="filter">
                <label>ClassName</label>
                <input
                  type="text"
                  className="form-control"
                  placeholder="e.g. first year"
                />
              </div>
              <div className="filter">
                <label>Division</label>
                <input
                  type="text"
                  className="form-control"
                  placeholder="e.g. A"
                />
              </div>
              <div className="filter fil-btn">
                <label></label>
                <button
                  type="submit"
                  className="btn btn-primary mb-2"
                  style={{ backgroundColor: "#4cadad" }}
                >
                  GO
                </button>
              </div>
            </div>

            <div className="table-content m-3">
              <table className="table table-hover">
                <thead>
                  <tr>
                    <th scope="col">
                      <div className="form-check">
                        <input
                          className="form-check-input position-static"
                          type="checkbox"
                          id="blankCheckbox"
                          value="option1"
                          aria-label="..."
                        />
                      </div>
                    </th>
                    <th scope="col">Teacher Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Subject</th>
                    <th scope="col">Designation</th>
                    <th scope="col">Shift</th>
                    <th>Edit/Delete</th>
                  </tr>
                </thead>
                <tbody>
                  {this.renderStudents(this.saveStudentProfileConfirm)}
                </tbody>
              </table>
            </div>
            <button
              className="btn btn-primary mb-2"
              onClick={() => this.setState({ addStudents: true })}
            >
              Add Teacher
            </button>
          </main>
        </div>
      );
    } else {
      return <Redirect push to="/" />;
    }
  };
}

const mapStateToProps = (state) => {
  console.log("students data changed maptoprops", state);
  return {
    isAuthenticated: state.isAuthenticated,
    userData: state.userData,
    teacherData: state.teacherData,
    removeTeacher: state.removeTeacher,
    updateTeacher: state.updateTeacher,
    createTeacher: state.createTeacher,
    profileImageUrl: state.studentImage.data,
  };
};

export default connect(mapStateToProps, { reloadTeachers })(TeachingStaffData);

// class TeachingStaffData extends React.Component {
//   state = {
//     myProfile: false,
//   };
//   myProfile = () => {
//     this.setState({
//       myProfile: true,
//     });
//   };
//   render() {
//     let data = this.props.userData;
//     let name = data["UserName"];
//     return (
//       <div className="main-content bg-light">
//         <Modal
//           open={this.state.myProfile}
//           onClose={() => this.setState({ myProfile: false })}
//           aria-labelledby="simple-modal-title"
//           aria-describedby="simple-modal-description"
//         >
//           <Draggable>
//             <div
//               tabIndex="-1"
//               style={{
//                 paddingLeft: 200,
//                 paddingTop: 200,
//                 width: 250,
//               }}
//             >
//               <MyProfile myProfile={data} />
//             </div>
//           </Draggable>
//         </Modal>
//         <header>
//           <h4>
//             <label htmlFor="nav-toggel">
//               <span>
//                 <i className="fa fa-bars" aria-hidden="true"></i>
//               </span>
//             </label>
//             <span className="name">TeachingStaffData</span>
//           </h4>

//           <div className="search-wrapper">
//             <span>
//               <i className="fa fa-search" aria-hidden="true"></i>
//             </span>
//             <input type="search" placeholder="Search here" />
//           </div>

//           <div className="user-wrapper" onClick={this.myProfile}>
//             <img
//               src={this.props.profileImageUrl}
//               width="50px"
//               height="50px"
//               alt=""
//             />
//             <div>
//               <h6>{name}</h6>
//               <small>Student</small>
//             </div>
//           </div>
//         </header>

//         <main>
//           <div className="filter-wrapper"></div>

//           <div className="table-content m-3">
//             <p>TeachingStaffData</p>
//           </div>
//         </main>
//       </div>
//     );
//   }
// }
// const mapStateToProps = (state) => {
//   return {
//     isAuthenticated: state.isAuthenticated,
//     userData: state.userData,
//     profileImageUrl: state.studentImage.data,
//   };
// };
// export default connect(mapStateToProps, null)(TeachingStaffData);

//
//import TeachingStaffData from "./TeachingStaffData";
//import AluminiData from "./AluminiData";
//import Help from "./Help";
//import StudentCorner from "./StudentCorner";
//import TeachingStaffData from "./TeachingStaffData";
