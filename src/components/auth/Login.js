import React from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import history from "../../history";
import { loginAction, removeAlert } from "../../actions";
import ForgotCodesForm from "./ForgotCodesForm";
import AlertComponent from "./AlertComponent";
import Modal from "@material-ui/core/Modal";
import Draggable from "react-draggable";
import ForgotUniversityCodes from "./ForgotUniversityCodes";
import Confirm from "./Confirm";
import { Field, reduxForm } from "redux-form";
import { SubmissionError } from "redux-form";
var { Auth, Storage } = require("aws-amplify");

const validate = (formValues) => {
  let error = {};
  if (!formValues.code) {
    error.code = "Please provide a University Code";
  }
  if (!formValues.username) {
    error.username = "Please provide a Email Code";
  }
  if (!formValues.password) {
    error.password = "Please provide a password";
  }
  return error;
};

class Login extends React.Component {
  state = {
    username: "",
    password: "",
    isSpinner: false,
    code: "",
    openCodeSubmit: false,
    openCodeForm: false,
    openConfirm: false,
    imageUrl: "",
  };

  _handleKeyDown = (e) => {
    if (e.key === "Enter") {
      console.log("Enter Presed");
      this.props.handleSubmit(this.onSubmit);
    }
  };

  // myFunction = async () => {
  //   console.log("hello world myFunction");
  //   try {
  //     let user = await Auth.signIn("vishalg485@gmail.com", "Vishal@1");
  //     console.log("user", user);
  //     const data = await Storage.get("campus-favicon.png", {
  //       contentType: "image/jpeg",
  //     });
  //     console.log("Data2", data);
  //     this.setState({ imageUrl: data });
  //   } catch (e) {
  //     console.log("error", e);
  //   }
  //   //console.log("data",data);
  // };
  renderError({ error, touched }) {
    //console.log("error, touched", error, touched);
    if (touched && error) {
      return (
        <div className="input-group">
          <div
            className="input-group-prepend"
            style={{ backgroundColor: "red" }}
          ></div>
          <p style={{ color: "red" }}>{error}</p>
        </div>
      );
    }
  }

  renderEmailInput = ({ input, placeholder, meta, type, label, iconClass }) => {
    //console.log("formValues", formProps);

    return (
      <div className="form-group">
        <label>{label}</label>
        <div className="input-group">
          <div className="input-group-prepend">
            <span className="input-group-text">
              <i className={iconClass} aria-hidden="true"></i>
            </span>
          </div>
          <input
            className="form-control"
            name="code"
            type={type}
            placeholder={placeholder}
            {...input}
          />
        </div>
        {this.renderError(meta)}
      </div>
    );
  };

  componentDidMount() {
    console.log("did mount");
    this.setState({ isSpinner: false });
  }

  onSubmit = (formValues) => {
    console.log("formValues", formValues);
    //e.preventDefault();

    this.setState({ isSpinner: true });

    return this.props
      .loginAction({
        username: formValues.username,
        password: formValues.password,
        code: formValues.code,
      })
      .then((data) => {
        this.setState({ isSpinner: false });
      })
      .catch((e) => {
        this.setState({ isSpinner: false });
        if (e === "Code is incorrect") {
          throw new SubmissionError({
            code: e,
          });
        } else if (e === "Incorrect username or password.") {
          throw new SubmissionError({
            password: e,
          });
        }
      });
  };

  onChange(e) {
    console.log("e", e.target.name, e.target.value);
    this.setState({ [e.target.name]: e.target.value });
  }
  removeCodesSubmitAddCodesForm = () => {
    console.log("CALLED CALLED CALLED");
    this.setState({ openCodeSubmit: false });
    this.setState({ openCodeForm: true });
  };
  removeCodesForm = () => {
    this.setState({ openCodeSubmit: false });
  };
  removeCodes = () => {
    this.setState({ openCodeForm: false });
  };
  render() {
    return (
      <div className="wrapper">
        <div className="login" onKeyDown={this._handleKeyDown}>
          {/* <img
            src={this.state.imageUrl}
            alt="vishal"
            style={{ width: "80%", marginBottom: "80px" }}
          /> */}
          <div className="row inner-wrap">
            <div className="col-md-6 p-0 bg-light">
              <div className="login-bg"></div>
            </div>
            <Modal
              open={this.state.openCodeForm}
              onClose={() => this.setState({ openCodeForm: false })}
              aria-labelledby="simple-modal-title"
              aria-describedby="simple-modal-description"
            >
              <Draggable>
                <div
                  style={{
                    paddingLeft: 600,
                    paddingTop: 300,
                    width: 250,
                  }}
                >
                  <ForgotUniversityCodes remove={this.removeCodes} />
                </div>
              </Draggable>
            </Modal>
            <Modal
              open={this.state.openCodeSubmit}
              onClose={this.removeCodesForm}
              aria-labelledby="simple-modal-title"
              aria-describedby="simple-modal-description"
            >
              <Draggable>
                <div
                  style={{
                    paddingLeft: 600,
                    paddingTop: 300,
                    width: 250,
                  }}
                >
                  <ForgotCodesForm
                    val="hello world"
                    check={this.removeCodesSubmitAddCodesForm}
                  />
                </div>
              </Draggable>
            </Modal>
            <Modal
              open={this.state.openConfirm}
              onClose={() => this.setState({ openConfirm: false })}
              aria-labelledby="simple-modal-title"
              aria-describedby="simple-modal-description"
            >
              <Draggable>
                <div
                  style={{
                    paddingLeft: 600,
                    paddingTop: 300,
                    width: 250,
                  }}
                >
                  <Confirm remove={this.removeCodes} />
                </div>
              </Draggable>
            </Modal>
            <div className="col-md-6 bg-white">
              <div className="inner-login">
                <div className="mb-8 campus-logo">
                  <img
                    src="image/campus-favicon.png"
                    alt="Logo"
                    style={{ width: "20%", marginBottom: "20px" }}
                  />
                </div>

                <div className="login-content">
                  <div
                    className="login-pane fade show active"
                    role="tabpanel"
                    id="login"
                  >
                    <p className="mt-6 mb-6 pt-2 pb-2">
                      Use your credentials to login into account.
                    </p>
                    <form onSubmit={this.props.handleSubmit(this.onSubmit)}>
                      <Field
                        label="Institute Code"
                        type="text"
                        name="code"
                        component={this.renderEmailInput}
                        placeholder="Institute Code"
                        iconClass="fa fa-university"
                      />
                      <Field
                        label="Email Address"
                        type="text"
                        name="username"
                        component={this.renderEmailInput}
                        placeholder="Email Address"
                        iconClass="fa fa-envelope-o"
                      />
                      <Field
                        label="Password"
                        type="password"
                        name="password"
                        component={this.renderEmailInput}
                        placeholder="Password"
                        iconClass="fa fa-lock"
                      />

                      <div className="row" style={{ paddingTop: "10px" }}>
                        <div className="col-12">
                          <button
                            type="submit"
                            className="btn btn-primary btn-block"
                          >
                            Login
                          </button>
                        </div>
                      </div>
                    </form>
                    <AlertComponent />
                    <div className="row">
                      <div
                        className="col-12"
                        style={{ paddingTop: "20px", paddingLeft: "20px" }}
                      >
                        <Link
                          to="/forgotpassword"
                          onClick={() => history.push("/forgotpassword")}
                        >
                          Forgot password?
                        </Link>
                      </div>
                      <div
                        className="col-12"
                        style={{ paddingTop: "10px", paddingLeft: "20px" }}
                      >
                        <Link
                          to="/register"
                          onClick={() => history.push("/register")}
                          // className="btn btn-primary btn-block"
                        >
                          Don't have account? Register Now
                        </Link>
                      </div>
                      <div
                        className="col-12"
                        style={{ paddingTop: "10px", paddingLeft: "20px" }}
                      >
                        <Link
                          to="/"
                          onClick={() => this.setState({ openConfirm: true })}
                          // className="btn btn-primary btn-block"
                        >
                          Reconfirm account ?
                        </Link>
                      </div>
                      <div
                        className="col-12"
                        style={{ paddingTop: "10px", paddingLeft: "20px" }}
                      >
                        <Link
                          to="/"
                          onClick={() =>
                            this.setState({ openCodeSubmit: true })
                          }
                        >
                          Forgot University Codes
                        </Link>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        {this.state.isSpinner ? (
          <div className="ui active inverted dimmer">
            <div className="ui loader"></div>
          </div>
        ) : (
          <React.Fragment />
        )}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  console.log("state", state.alertUser);
  return {
    alertObject: state.alertUser,
  };
};

//export default connect(mapStateToProps, { loginAction, removeAlert })(Login);
const formWrapped = reduxForm({
  form: "loginForm",
  validate,
})(Login);

export default connect(mapStateToProps, { loginAction, removeAlert })(
  formWrapped
);

/*
this the component which gets loaded whe we call mthe page for the first time.
Modal are the pops that gets called for reconfirm account and forgot university codes.
All the data is received in the state variable, all the data in the form gets stored over there,
whenever there is change in the state variable the component gets rerendered.
*/
