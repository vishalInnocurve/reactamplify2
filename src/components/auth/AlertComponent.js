import React from "react";
import { connect } from "react-redux";

class AlertComponent extends React.Component {
  render() {
    if (this.props.alertObject.isAlert) {
      return <p style={{ color: "red" }}>{this.props.alertObject.msg}</p>;
    } else {
      return <p></p>;
    }
  }
}

const mapStateToProps = (state) => {
  console.log("state alert ", state.alertUser.msg);
  return {
    alertObject: state.alertUser,
  };
};
export default connect(mapStateToProps)(AlertComponent);

/*
when en error occurs in our system this component gets called.


*/
