import React from "react";
import { connect } from "react-redux";
import ReactDOM from "react-dom";
import history from "../../history";
import { anotherUserSignUp } from "../../actions";
// var UniversityData = [
//   { name: "abc1", code: 1 },
//   { name: "abc2", code: 2 },
//   { name: "abc3", code: 3 },
//   { name: "abc4", code: 4 },
// ];

class RegisterAnother extends React.Component {
  renderUniversityData() {
    console.log("this.props", this.props.anotherUser.apiDetails.data);
    return this.props.anotherUser.apiDetails.data.map((item) => {
      return (
        <div className="item" key={item.UniCode}>
          <div className="left aligned content">{item.Uni_Name}</div>
          <div className="right aligned content">{item.UniCode}</div>
          <div className="right aligned content">{item.UserRole}</div>
        </div>
      );
    });
  }
  onSubmit() {
    console.log("main submit called", this.props.anotherUser.formData);
    this.props.anotherUserSignUp(this.props.anotherUser.formData);
  }
  render() {
    return ReactDOM.createPortal(
      <div
        onClick={() => history.push("/")}
        className="ui dimmer modals visible active"
      >
        <div
          onClick={(e) => e.stopPropagation()}
          className="ui standard modal visible active"
        >
          <div className="header">
            User is Already Registered for these University
          </div>
          <div className="content">
            <div className="ui divided items">
              <div className="item">
                <div className="left aligned content">Collge</div>
                <div className="left aligned content">Role</div>
                <div className="right aligned content">Code</div>
              </div>
              {this.renderUniversityData()}
            </div>
          </div>
          <div className="actions">
            <button className="ui button" onClick={() => this.onSubmit()}>
              Register
            </button>
            <button className="ui button" onClick={() => history.push("/")}>
              Cancel
            </button>
          </div>
        </div>
      </div>,
      document.querySelector("#modal")
    );
  }
}
const mapStateToProps = (state) => {
  console.log("state another User Request", state.anotherUser);
  return {
    anotherUser: state.anotherUser,
  };
};

export default connect(mapStateToProps, { anotherUserSignUp })(RegisterAnother);

/*
this component gets rendered when we register when a user which is already registered tries to re register itself for another university.


*/
