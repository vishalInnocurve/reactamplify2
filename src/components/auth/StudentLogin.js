import React from "react";

import { Field, reduxForm } from "redux-form";
import { connect } from "react-redux";
import { registerUser } from "../../actions";

import { load } from "../../actions";

function checkPassword(str) {
  var re = /^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,}$/;
  return re.test(str);
}

const validate = (formValues) => {
  let error = {};
  if (!formValues.password) {
    error.password = "Please provide a password";
  }
  if (!formValues.confirm_password) {
    error.confirm_password = "Please provide a password";
  }
  if (!checkPassword(formValues.password)) {
    error.password =
      "* Password should have minimum of 7 character with at least one uppercase, one lowercase a number and one special symbol";
  }
  if (
    formValues.password &&
    formValues.confirm_password &&
    formValues.password !== formValues.confirm_password
  ) {
    error.confirm_password = "Both password should match";
  }
  return error;
};

class StudentLogin extends React.Component {
  renderError({ error, touched }) {
    //console.log("error, touched", error, touched);
    if (touched && error) {
      return (
        <div className="input-group">
          <div
            className="input-group-prepend"
            style={{ backgroundColor: "red" }}
          ></div>
          <p style={{ color: "red" }}>{error}</p>
        </div>
      );
    }
  }

  renderEmailInput = ({ input, placeholder, meta, type, label }) => {
    //console.log("formValues", formProps);

    return (
      <div className="form-group">
        <label>{label}</label>
        <div className="input-group">
          <div className="input-group-prepend"></div>
          <input
            className="form-control"
            type={type}
            placeholder={placeholder}
            {...input}
          />
        </div>
        {this.renderError(meta)}
      </div>
    );
  };

  onSubmit = async (formData) => {
    console.log("onSubmit", formData);
    console.log("student submit", this.props.location.state);
    this.props.registerUser({
      email: this.props.location.state.email,
      old_password: this.props.location.state.password,
      password: formData.password,
    });
  };

  componentDidMount() {
    this.props.load({ email: this.props.location.state.detail });
  }

  render() {
    console.log("this.props", this.props);
    console.log("this.props.state", this.props.location.state.detail);
    return (
      <form
        className="ui form"
        onSubmit={this.props.handleSubmit(this.onSubmit)}
      >
        <Field
          label="New Password"
          type="password"
          name="password"
          component={this.renderEmailInput}
          placeholder="Password"
        />
        <Field
          label="Confirm password"
          type="password"
          name="confirm_password"
          component={this.renderEmailInput}
          placeholder="Confirm Password"
        />
        <button className="ui button" type="submit">
          Submit
        </button>
      </form>
    );
  }
}

const formWrapped = reduxForm({
  form: "registerStudent",
  validate,
})(StudentLogin);

const mapStateToProps = (state) => {
  console.log("state", state.formInitialValues.data);
  if (state.formInitialValues.data) {
    return {
      initialValues: {
        email: state.formInitialValues.data.email, //"vishal",
      },
    };
  } else {
    return {
      initialValues: {},
    };
  }
};
export default connect(mapStateToProps, { registerUser, load })(formWrapped);

/*
this is form which needs to be filled when we try to create a Student
currently under development.
*/
