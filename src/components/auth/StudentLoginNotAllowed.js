import React from "react";
import history from "../../history";
import Modal from "@material-ui/core/Modal";
import Draggable from "react-draggable";

class StudentLoginPopUp extends React.Component {
  render() {
    return (
      <div onClick={() => history.push("/")}>
        <div
          onClick={(e) => e.stopPropagation()}
          className="ui standard modal visible active"
        >
          <div className="content">Student Login Not Allowed</div>
        </div>
      </div>
    );
  }
}

class StudentLoginNotAllowed extends React.Component {
  state = {
    openCodeForm: true,
  };
  changeFunc = () => {
    console.log("hello");
    history.push("/");
    this.setState({ openCodeForm: !this.state.openCodeForm });
  };
  render() {
    return (
      <Modal
        open={this.state.openCodeForm}
        onClose={() => this.changeFunc()}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        <Draggable>
          <div
            style={{
              paddingLeft: 600,
              paddingTop: 300,
              width: 250,
            }}
          >
            <StudentLoginPopUp />
          </div>
        </Draggable>
      </Modal>
    );
  }
}

export default StudentLoginNotAllowed;
