import React from "react";
import ReactDOM from "react-dom";
import App from "./components";
import thunk from "redux-thunk";
import { Provider } from "react-redux";
//import { createStore, applyMiddleware } from "redux";
import reducer from "./reducers";
import "crypto-js/lib-typedarrays";
import Amplify from "aws-amplify";
import config from "./config.json";
import { createStore, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension/logOnlyInProduction";
const composeEnhancers = composeWithDevTools({
  // options like actionSanitizer, stateSanitizer
});

Amplify.configure({
  Auth: {
    identityPoolId: "us-east-2:d9bd1bdc-aeb3-4e20-a4a2-c63ff36327ea",
    region: config.cognito.REGION,
    userPoolId: config.cognito.USER_POOL_ID,
    userPoolWebClientId: config.cognito.APP_CLIENT_ID,
    authenticationFlowType:
      "USER_SRP_AUTH" | "USER_PASSWORD_AUTH" | "CUSTOM_AUTH",
  },
  API: {
    endpoints: [
      {
        name: "retrieveInformationAPI1",
        endpoint: "https://ys546abxge.execute-api.us-east-2.amazonaws.com/dev",
      },
    ],
  },
  Storage: {
    AWSS3: {
      bucket: "innocurveuniversity", //REQUIRED -  Amazon S3 bucket name
      region: "us-east-2", //OPTIONAL -  Amazon service region
      identityPoolId: "us-east-2:d9bd1bdc-aeb3-4e20-a4a2-c63ff36327ea",
    },
  },
  aws_appsync_graphqlEndpoint:
    "https://bucmhxieufhsvc6zrspqnccmf4.appsync-api.us-east-2.amazonaws.com/graphql",
  aws_appsync_region: "us-east-2",
  aws_appsync_authenticationType: "API_KEY",
  aws_appsync_apiKey: "da2-qxrou526gba77cbqwqy6ysgid4",
});

//const store = createStore(reducer, applyMiddleware(thunk));

const middleware = [thunk];
const store = createStore(
  reducer,
  composeEnhancers(applyMiddleware(...middleware))
);

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.querySelector("#root")
);

/*
this is the first file which gets rendered when we first load our app,
We also have amplify configure in this file which is basically the bridge between the 
React and AWS, all the Api and Cognito Configuration are being done over here. 
this file renders the App component which contains all the routes being used in React.
*/
