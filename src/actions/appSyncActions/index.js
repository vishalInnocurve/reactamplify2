import { API, graphqlOperation } from "aws-amplify";
import { studentsData, teachersData } from "../../mutation";

//whenever students gets updates we call this action to reload all the students data.
export const reloadStudents = (UniCode) => {
  return async function (dispatch) {
    try {
      let input2 = {
        id: UniCode,
      };
      const result = await API.graphql(graphqlOperation(studentsData, input2));
      console.log("LAMBDA reloadStudents Action", result.data.studentData);
      dispatch({ type: "STUDENTS_DATA", payload: result.data.studentData });
    } catch (e) {
      console.log("FAILED LOGOUT student reload failed students", e);
    }
  };
};

export const removeStudent = (email) => {
  return async function (dispatch) {
    dispatch({ type: "REMOVE_STUDENT", payload: email });
  };
};

export const createStudent = (createStudent) => {
  return async function (dispatch) {
    dispatch({ type: "CREATE_STUDENT", payload: createStudent });
  };
};

export const updateStudent = (updateStudent) => {
  return async function (dispatch) {
    dispatch({ type: "UPDATE_STUDENT", payload: updateStudent });
  };
};

export const reloadTeachers = (UniCode) => {
  return async function (dispatch) {
    try {
      let input2 = {
        id: UniCode,
      };
      const result = await API.graphql(graphqlOperation(teachersData, input2));
      console.log("LAMBDA reloadTeachers Action", result.data.teacherData);
      dispatch({ type: "TEACHERS_DATA", payload: result.data.teacherData });
    } catch (e) {
      console.log("FAILED LOGOUT", e);
    }
  };
};

export const removeTeacher = (email) => {
  return async function (dispatch) {
    dispatch({ type: "REMOVE_TEACHER", payload: email });
  };
};

export const createTeacher = (createStudent) => {
  return async function (dispatch) {
    dispatch({ type: "CREATE_TEACHER", payload: createStudent });
  };
};

export const updateTeacher = (updateStudent) => {
  return async function (dispatch) {
    dispatch({ type: "UPDATE_TEACHER", payload: updateStudent });
  };
};
