import { combineReducers } from "redux";
import { reducer as formReducer } from "redux-form";

const isAuthenticated = function (isAuth = false, action) {
  if (action.type === "IS_AUTHENTICATED") {
    return action.payload;
  }
  return isAuth;
};

const userData = function (user = null, action) {
  if (action.type === "LOGIN_SUCCESS") {
    return action.payload;
  }
  return user;
};

const anotherUser = function (user = null, action) {
  if (action.type === "ANOTHER_USER") {
    return action.payload;
  }
  return user;
};

const alertUser = function (alert = { isAlert: false, msg: "" }, action) {
  if (action.type === "LOGIN_FAILED" || action.type === "REGISTER_FAILED") {
    console.log("check");
    let obj = { isAlert: true, msg: action.payload };
    return obj;
  }
  if (action.type === "REMOVE_ALERT") {
    console.log("check2");
    let obj = { isAlert: false, msg: "" };
    return obj;
  }
  return alert;
};

const reducerAdminProfile = (state = {}, action) => {
  switch (action.type) {
    case "LOAD":
      return {
        data: action.data,
      };
    default:
      return state;
  }
};

const reducerStudentProfile = (state = {}, action) => {
  switch (action.type) {
    case "LOAD_STUDENT":
      return {
        data: action.data,
      };
    default:
      return state;
  }
};

const reducerTeacherProfile = (state = {}, action) => {
  switch (action.type) {
    case "LOAD_TEACHER":
      return {
        data: action.data,
      };
    default:
      return state;
  }
};

const removeStudent = (state = {}, action) => {
  switch (action.type) {
    case "REMOVE_STUDENT":
      return {
        data: action.payload,
      };
    default:
      return state;
  }
};

const removeTeacher = (state = {}, action) => {
  switch (action.type) {
    case "REMOVE_TEACHER":
      return {
        data: action.payload,
      };
    default:
      return state;
  }
};

const updateStudent = (state = {}, action) => {
  switch (action.type) {
    case "UPDATE_STUDENT":
      return {
        data: action.payload,
      };
    default:
      return state;
  }
};

const updateTeacher = (state = {}, action) => {
  switch (action.type) {
    case "UPDATE_TEACHER":
      return {
        data: action.payload,
      };
    default:
      return state;
  }
};
const createStudent = (state = {}, action) => {
  switch (action.type) {
    case "CREATE_STUDENT":
      return {
        data: action.payload,
      };
    default:
      return state;
  }
};

const createTeacher = (state = {}, action) => {
  switch (action.type) {
    case "CREATE_TEACHER":
      return {
        data: action.payload,
      };
    default:
      return state;
  }
};

const studentImage = (state = "", action) => {
  switch (action.type) {
    case "PROFILE_IMAGE":
      return {
        data: action.payload,
      };
    default:
      return state;
  }
};

const studentData = (state = {}, action) => {
  switch (action.type) {
    case "STUDENTS_DATA":
      return {
        data: action.payload,
      };
    default:
      return state;
  }
};

const teacherData = (state = {}, action) => {
  switch (action.type) {
    case "TEACHERS_DATA":
      return {
        data: action.payload,
      };
    default:
      return state;
  }
};

export default combineReducers({
  isAuthenticated,
  userData,
  alertUser,
  form: formReducer,
  removeStudent,
  removeTeacher,
  createTeacher,
  updateStudent,
  createStudent,
  updateTeacher,
  anotherUser,
  teacherData,
  studentData,
  studentImage,
  formInitialValues: reducerAdminProfile, //loads the initial values of admin profile
  studentProfileValues: reducerStudentProfile, //loads the initial values of students
  teacherProfileValues: reducerTeacherProfile,
});

/*
this file is responsible for reloding the entire component when certain event occurs.
the dispatch from actions reaches over here, and then passed to the respective component.


*/
