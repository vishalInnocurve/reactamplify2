export const createStudent = "student";

export const deleteStudentMutation = `mutation deleteStudentMutation($input:studentDeletionChange){
  deleteStudentMutation(input: $input){
    email
  }
}`;

export const updateStudentMutation = `mutation updateStudent($input:studentUpdateInput){
  updateStudent(input:$input){
    email
  	name
  	phoneNumber
  	adminEmail
  	adminCode
   	registrationNumber
  	parentPhoneNumber
  	rollNo
  	dateOfbirth
    parentName
    address
  	error
  }
}`;

export const studentsData = `
query studentData($id: String!){
  studentData(collegeId : $id){
    name
    email
    phoneNumber
  	rollNo
  	registrationNumber
  	userdob
    designation
    parentName
    parentPhoneNumber
    address
  }
}`;

export const teachersData = `
query teacherData($id: String!){
  teacherData(collegeId : $id){
    name
    email
    phoneNumber
  	rollNo
  	registrationNumber
  	userdob
    designation
  }
}`;

export const deleteTeacherMutation = `mutation deleteTeacherMutation($input:studentDeletionChange){
  deleteTeacherMutation(input: $input){
    email
  }
}`;

export const updateTeacherMutation = `mutation updateTeacher($input:teacherUpdateInput){
  updateTeacher(input:$input){
    email
  	name
  	phoneNumber
  	adminEmail
  	adminCode
   	registrationNumber
  	rollNo
  	dateOfbirth
  	error
  }
}`;
